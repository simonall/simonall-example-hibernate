package com.simonall.constant;

import java.util.UUID;

/**
 * 常量Value
 * 
 * @author simonall
 */
public class Value {
	
	/**
	 * 系统编码
	 * 
	 */
	public static final String CHARSET = "UTF-8";
	
	/**
	 * 启动随机值
	 * 
	 */
	public static final String DYNAMIC_CODE = UUID.randomUUID().toString();
	
	/**
	 * BLANK
	 * 
	 */
	public static final String BLANK = "";
	
	/**
	 * SUCCESS
	 * 
	 */
	public static final String SUCCESS = "SUCCESS";
	
	/**
	 * FAIL
	 * 
	 */
	public static final String FAIL = "FAIL";
	
}
