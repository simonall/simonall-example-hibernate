package com.simonall.enums;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.simonall.exception.BusinessException;
import com.simonall.exception.BusinessIllegalException;

/**
 * 响应码
 * 
 * @author simonall
 */
public enum ResponseCode {

	/**
	 * 成功
	 * 
	 */
	SUCCESS,

	/**
	 * 失败
	 * 
	 */
	FAIL,

	;

	private String code = this.name();

	public String getMessage() {
		return null;
	}

	public String getCode() {
		return code;
	}

	public String toJson() {
		return JSON.toJSONString(toMap());
	}

	public Map<String, String> toMap() {
		Map<String, String> result = new HashMap<String, String>();
		result.put(Keys.code.name(), this.getCode());
		result.put(Keys.message.name(), this.getMessage());
		return result;
	}

	/**
	 * Business Exception
	 * 
	 * @return
	 */
	public BusinessException newBusinessException() {
		return new BusinessException(this);
	}

	/**
	 * Business Exception
	 * 
	 * @return
	 */
	public BusinessIllegalException newBusinessIllegalException() {
		return new BusinessIllegalException(this);
	}

}
