package com.simonall.exception;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.simonall.constant.Value;
import com.simonall.enums.ResponseCode;
import com.simonall.enums.Keys;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = -8539591719069825182L;
	
	/**
	 * 单描述构造器
	 * 
	 * @param message
	 */
	public BusinessException(String message) {
		super(message);
		this.code = Value.FAIL;
		this.message = message;
	}

	/**
	 * 完全参数构造器
	 * 
	 * @param code
	 * @param message
	 */
	public BusinessException(String code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}
	
	/**
	 * 完全参数构造器
	 * 
	 * @param code
	 * @param message
	 */
	public BusinessException(ResponseCode message) {
		super(message.getMessage());
		this.code = message.name();
		this.message = message.getMessage();
	}
	
	private String code;
	
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public final String toJson() {
		return JSON.toJSONString(toMap());
	}

	public final Map<String, String> toMap() {
		Map<String, String> result = new HashMap<String, String>();
		result.put(Keys.code.name(), getCode());
		result.put(Keys.message.name(), getMessage());
		return result;
	}
}
