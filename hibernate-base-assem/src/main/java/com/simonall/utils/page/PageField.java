package com.simonall.utils.page;

import java.io.Serializable;

/**
 * 查询字段
 * 
 * @author simon
 */
public class PageField implements Serializable {

	private static final long serialVersionUID = 428411091899612562L;

	// 查询语句
	private String statement;
	
	// 查询条件
	private Object value;

	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
	
}
