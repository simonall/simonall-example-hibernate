package com.simonall.utils.page;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Table;

public class PageFilter {

	// 被分页的对象
	private Class<?> clazz;

	// 要查找第几页
	private Integer page = 1;

	// 每页显示多少条
	private Integer pageSize = 10;

	// 查询条件
	private List<PageField> condition = new ArrayList<PageField>();

	// 排序语句
	private String orderBy;

	public PageFilter(Class<?> clazz) {
		super();
		this.clazz = clazz;
	}

	public List<PageField> getCondition() {
		return condition;
	}

	public void setCondition(List<PageField> condition) {
		this.condition = condition;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderBy() {
		return orderBy;
	}
	
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	
	/**
	 * 新增一个查询字段
	 * 
	 * @param field
	 */
	private void setField(PageField field) {
		condition.add(field);
	}

	/**
	 * 添加分页条件
	 * 
	 * @param statement 条件语句
	 * @param value     查询条件值
	 */
	public PageFilter addFilter(String statement, Object value) {
		PageField field = new PageField();
		field.setStatement(statement);
		field.setValue(value);
		this.setField(field);
		return this;
	}

	/**
	 * 排序条件
	 * 
	 * @param value 排序语句
	 */
	public void orderBy(String value) {
		this.orderBy = value;
	}
	
	/**
	 * 获取当前对象类名
	 * 
	 * @return
	 */
	public String getClassName() {
		return clazz.getSimpleName();
	}
	
	/**
	 * 获取当前对象Table注解表名
	 * 
	 * @return
	 */
	public String getTableName() {
		String result = new String();
		Annotation[] annotation = clazz.getAnnotations();
		for(Annotation tation : annotation) {
			if(Objects.equals(tation.annotationType().getSimpleName(), Table.class.getSimpleName())) {
				result = ((Table)tation).name();
			}
		}
		return result;
	}
}
