package com.simonall.utils.page;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.simonall.constant.SQL;
import com.simonall.constant.Value;
import com.simonall.enums.CHS;

/**
 * Page Helper
 * 
 * @author simon
 */
public class PageHelper implements Serializable {

	private static final long serialVersionUID = 3954139509930824175L;

	private PageFilter filter;

	private boolean appendCondSwitch = true;

	public PageHelper(PageFilter filter) {
		this.filter = filter;
	}

	public PageHelper(PageFilter filter, boolean coverCondSwitch) {
		this.filter = filter;
		this.appendCondSwitch = coverCondSwitch;
	}

	/**
	 * 追加条件SQL开关
	 * 
	 * @return
	 */
	public boolean isAppendCondSwitch() {
		return appendCondSwitch;
	}

	/**
	 * 获取页码
	 * 
	 * @return
	 */
	public Integer getStartIndex() {
		Integer pageNo = filter.getPage();
		Integer pageSize = filter.getPageSize();
		Integer start = (--pageNo) * pageSize;
		if (start < 0) {
			start = 0;
		}
		return start;
	}
	
	/**
	 * 当前页码
	 * 
	 * @return
	 */
	public Integer getPage() {
		return filter.getPage();
	}

	/**
	 * 每页数量
	 * 
	 * @return
	 */
	public Integer getPageSize() {
		return filter.getPageSize();
	}

	/**
	 * 获取条件语句
	 * 
	 * @return
	 */
	public StringBuilder getStatements() {
		StringBuilder statements = new StringBuilder();
		if (this.isAppendCondSwitch()) {
			List<PageField> condition = filter.getCondition();
			if (condition != null && condition.size() > 0) {
				Integer length = condition.size();
				for (int i = 0; i < length; i++) {
					PageField field = condition.get(i);
					String statement = field.getStatement();
					if(i == 0) {
						statements.append(statement.replace(SQL.AND.trim(), Value.BLANK).replace(SQL.OR.trim(), Value.BLANK)
								.replace(SQL.AND.trim().toLowerCase(), Value.BLANK).replace(SQL.OR.trim().toLowerCase(), Value.BLANK));
					}else {
						Integer andIndex = statement.toUpperCase().indexOf(SQL.AND.trim());
						Integer orIndex = statement.toUpperCase().indexOf(SQL.OR.trim());
						if(andIndex > -1 || orIndex > -1) {
							statements.append(statement);
						}else {
							statements.append(SQL.AND).append(statement);
						}
					}
 				}
			}
		}
		return statements;
	}
	
	/**
	 * 追加条件语句
	 * 
	 * @return
	 */
	public StringBuilder getStatements(StringBuilder statement) {
		if (this.isAppendCondSwitch()) {
			StringBuilder appendState = getStatements();
			if(appendState != null && appendState.length() > 0) {
				String ment = String.valueOf(statement);
				if(ment.toUpperCase().indexOf(SQL.WHERE.trim()) > -1) {
					String[] statementCut = ment.split(SQL.WHERE.trim());
					if(statementCut.length < 2) {
						statementCut = ment.split(SQL.WHERE.trim().toLowerCase());
					}
					return new StringBuilder(statementCut[0]).append(SQL.WHERE).append(appendState).append(SQL.AND).append(statementCut[1]);
				}else {
					String className = filter.getClassName();
					String tableName = filter.getTableName();
					String[] statementCut = null;
					if(statement.indexOf(className) > -1) { // HQL
						statementCut = ment.split(className);
						return new StringBuilder(statementCut[0]).append(className).append(SQL.WHERE).append(appendState).append(statementCut[1]);
					}else if(statement.indexOf(tableName) > -1) { // SQL
						statementCut = ment.split(tableName);
						return new StringBuilder(statementCut[0]).append(tableName).append(SQL.WHERE).append(appendState).append(statementCut[1]);
					}
				}
			}
		}
		return statement;
	}
	
	
	/**
	 * 追加查询条件值
	 * 
	 * @return
	 */
	public void appendValue(Map<String, Object> values) {
		if (this.isAppendCondSwitch()) {
			List<PageField> condition = filter.getCondition();
			if (condition != null && condition.size() > 0) {
				for (PageField field : condition) {
					String statement = field.getStatement();
					int ident = statement.indexOf(CHS.colon.getSymbol());
					values.put(statement.substring(++ident).trim(), field.getValue());
 				}
			}
		}
	}
	
	/**
	 * 追加排序条件值
	 * 
	 * @return
	 */
	public StringBuilder appendOrderBy(StringBuilder statement) {
		if (this.isAppendCondSwitch() && StringUtils.hasText(filter.getOrderBy())) {
			Pattern pattern = Pattern.compile("order\\s*by[\\w|\\W|\\s|\\S]*", Pattern.CASE_INSENSITIVE);
			Matcher match = pattern.matcher(String.valueOf(statement));
			if(match.find()) { // 找到
				StringBuffer buffer = new StringBuffer();
				while (match.find()) {
					match.appendReplacement(buffer, filter.getOrderBy());
				}
				match.appendTail(buffer);
				return new StringBuilder(buffer);
			}else { // 未找到
				return statement.append(filter.getOrderBy());
			}
		}else {
			return statement;
		}
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		StringBuilder sql = new StringBuilder("FROM t_user where 1 = 1 order by createTime DESC ");
		PageFilter filter = new PageFilter(String.class);
		//filter.setOrderBy("order by id DESC ");
		filter.addFilter("userName = :userName", "李进全");
		PageHelper helper = new PageHelper(filter);
		//sql = helper.appendOrderBy(sql);
		//System.out.println(sql);
		
		Map<String, Object> values = new HashMap<String, Object>();
		helper.appendValue(values);
		System.out.println(values);
		
	}
	
	/**
	 * 绑定查询条件到本地
	 * 
	 * @param filter
	 */
	public static void bindingLocalThread(PageFilter filter) {
		PageHelper pageHelper = new PageHelper(filter);
		PageThreadLocal.setLocalPage(pageHelper);
	}
}
