package com.simonall.utils.page;

public class PageThreadLocal {
	
	private static final ThreadLocal<PageHelper> PAGE_THREAD_LOCAL = new ThreadLocal<PageHelper>();
	
    public static PageHelper getLocalPage() {
        return PAGE_THREAD_LOCAL.get();
    }

    public static void setLocalPage(PageHelper page) {
        PAGE_THREAD_LOCAL.set(page);
    }
    
    public static void clearLocalPage() {
        PAGE_THREAD_LOCAL.remove();
    }
}
