package com.simonall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * 配置中心
 * 
 * @author simon
 */
@EnableConfigServer
@EnableDiscoveryClient
@SpringBootApplication
public class MybatisConfigApp {
    	
	public static void main(String[] args) {
		SpringApplication.run(MybatisConfigApp.class, args);
	}
	
}
