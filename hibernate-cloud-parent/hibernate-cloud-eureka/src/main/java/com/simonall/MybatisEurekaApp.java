package com.simonall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 注册中心
 * 
 * @author simon
 */
@EnableEurekaServer
@SpringBootApplication
public class MybatisEurekaApp {

	public static void main(String[] args) {
		SpringApplication.run(MybatisEurekaApp.class, args);
	} 
	
}
