package com.simonall.mysql;

import com.simonall.member.model.User;
import com.simonall.mysql.dao.BaseDao;

public interface UserDao extends BaseDao<User> {

}
