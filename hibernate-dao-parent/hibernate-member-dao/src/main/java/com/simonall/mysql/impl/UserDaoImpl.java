package com.simonall.mysql.impl;

import org.springframework.stereotype.Repository;

import com.simonall.member.model.User;
import com.simonall.mysql.UserDao;
import com.simonall.mysql.dao.impl.BaseDaoImpl;

@Repository(value = "userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
	
}
