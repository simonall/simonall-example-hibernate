package com.simonall.dao.mysql;

import com.simonall.mysql.dao.BaseDao;
import com.simonall.mysql.model.SystemLog;

public interface SystemLogDao extends BaseDao<SystemLog> {

}
