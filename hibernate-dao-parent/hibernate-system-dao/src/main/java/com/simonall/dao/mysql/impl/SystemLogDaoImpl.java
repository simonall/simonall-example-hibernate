package com.simonall.dao.mysql.impl;

import org.springframework.stereotype.Repository;

import com.simonall.dao.mysql.SystemLogDao;
import com.simonall.mysql.dao.impl.BaseDaoImpl;
import com.simonall.mysql.model.SystemLog;

@Repository(value = "systemLogDao")
public class SystemLogDaoImpl extends BaseDaoImpl<SystemLog> implements SystemLogDao {
	
	
}
