package com.simonall.mongo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;

/**
 * Init Redis
 * 
 * @author simon
 */
@Configuration
@Order(value = Integer.MIN_VALUE)
@ImportResource(locations = { "classpath:spring-data-mongo.xml" })
public class MongoConfig {

}
