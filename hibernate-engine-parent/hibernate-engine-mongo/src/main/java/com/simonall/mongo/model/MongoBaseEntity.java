package com.simonall.mongo.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

public class MongoBaseEntity implements Serializable {

	private static final long serialVersionUID = -4656044035894707211L;
	
	@Id
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
