package com.simonall.mysql.dao.impl;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import com.simonall.constant.SQL;
import com.simonall.enums.CHS;
import com.simonall.mysql.dao.BaseDao;
import com.simonall.utils.page.Page;
import com.simonall.utils.page.PageHelper;
import com.simonall.utils.page.PageThreadLocal;

/**
 * BaseDaoImpl
 * 
 * @author simonall
 */
@SuppressWarnings("all")
public class BaseDaoImpl<T> extends AbstractBaseDao<T> implements BaseDao<T> {

	@Override
	public void save(T entity) {
		super.currentSession().save(entity);
	}

	@Override
	public void deleteById(String... id) {
		for (String i : id) {
			if (i != null && i.length() > BigInteger.ZERO.intValue()) {
				T record = super.currentSession().load(clazz, i);
				if (record != null) {
					super.currentSession().delete(record);
				}
			}
		}
	}

	@Override
	public void update(T entity) {
		super.currentSession().update(entity);
	}

	@Override
	public void saveOrUpdate(T entity) {
		super.currentSession().saveOrUpdate(entity);
	}

	@Override
	public T getById(Serializable id) {
		return (T) super.currentSession().get(clazz, id);
	}

	@Override
	public List<T> list() {
		StringBuilder hql = new StringBuilder("FROM " + clazz.getName());
		return super.hqlQuery(hql, null).list();
	}

	@Override
	public T get(String field, Object value) {
		Assert.hasText(field, "field must not be empty");
		Assert.notNull(value, "value is required");
		
		Map<String, Object> values = new HashMap<>();
		values.put("value", value);
		
		String hql = "FROM " + clazz.getName() + " AS model WHERE model." + field + " = :value";
		return super.hqlQuery(hql, values).uniqueResult();
	}

	@Override
	public List<T> list(String field, Object value) {
		Assert.hasText(field, "field must not be empty");
		Assert.notNull(value, "value is required");
		
		Map<String, Object> values = new HashMap<>();
		values.put("value", value);
		
		String hql = "FROM " + clazz.getName() + " AS model where model." + field + " = :value";
		return super.hqlQuery(hql, values).list();
	}

	@Override
	public boolean isExist(String field, Object value) {
		Assert.hasText(field, "field must not be empty");
		Assert.notNull(value, "value is required");
		
		T object = get(field, value);
		return (object != null);
	}
	
	@Override
	public List<T> listByHql(StringBuilder hql, Map<String, Object> values) {
		PageHelper pageHelper = PageThreadLocal.getLocalPage();
		if (pageHelper != null && pageHelper.isAppendCondSwitch()) {
			hql = pageHelper.getStatements(hql);
			hql = pageHelper.appendOrderBy(hql);
			pageHelper.appendValue(values);
		}
		Query<T> query = super.hqlQuery(hql, values);
		if (pageHelper != null) { // 强行分页
			PageThreadLocal.clearLocalPage();
			int startIndex = pageHelper.getStartIndex();
			int pageSize = pageHelper.getPageSize();
			query.setFirstResult(startIndex).setMaxResults(pageSize);
		}
		return query.list();
	}

	@Override
	public T getUniqueByHql(StringBuilder hql, Map<String, Object> values) {
		Query<T> query = super.hqlQuery(hql, values);
		return query.uniqueResult();
	}

	@Override
	public <R> R getUniqueObjectByHql(StringBuilder hql, Map<String, Object> values) {
		Query<?> query = super.hqlQuery(hql, values);
		return (R) query.uniqueResult();
	}

	@Override
	public Page<T> pageByHql(StringBuilder hql, Map<String, Object> values) {
		String totalState = "SELECT COUNT(*) " + removeFromBefore(removeOrder(hql));
		long totalCount = getUniqueObjectByHql(new StringBuilder(totalState), values);
		
		PageHelper pageHelper = PageThreadLocal.getLocalPage();
		Page<T> page = new Page<T>(pageHelper.getPage(), pageHelper.getPageSize());
		if (pageHelper != null && pageHelper.isAppendCondSwitch()) {
			hql = pageHelper.getStatements(hql);
			hql = pageHelper.appendOrderBy(hql);
			pageHelper.appendValue(values);
		}

		Query<T> query = super.hqlQuery(hql, values);
		if (pageHelper != null) { // 强行分页
			PageThreadLocal.clearLocalPage();
			int startIndex = pageHelper.getStartIndex();
			int pageSize = pageHelper.getPageSize();
			query.setFirstResult(startIndex).setMaxResults(pageSize);
		}
		page.setTotalCount(totalCount);
		page.setResult(query.list());
		return page;
	}
	
	@Override
	public T getUniqueBySql(StringBuilder sql, Map<String, Object> values) {
		Query<T> query = super.entitySqlQuery(sql, values);
		return query.uniqueResult();
	}

	@Override
	public List<T> listBySql(StringBuilder sql, Map<String, Object> values) {
		Query<T> query = super.entitySqlQuery(sql, values);
		return query.list();
	}

	@Override
	public Page<T> pageBySql(StringBuilder sql, Map<String, Object> values) {
		StringBuilder totalCountSql = new StringBuilder("SELECT COUNT(*) " + removeFromBefore(removeOrder(sql)));
		long totalCount = ((BigInteger) getUniqueObjectBySql(totalCountSql, values)).longValue();
		
		PageHelper pageHelper = PageThreadLocal.getLocalPage();
		Page<T> page = new Page<T>(pageHelper.getPage(), pageHelper.getPageSize());
		page.setTotalCount(totalCount);

		if (pageHelper != null && pageHelper.isAppendCondSwitch()) {
			StringBuilder nestatement = pageHelper.getStatements(sql);
			if (StringUtils.hasLength(nestatement)) {
				sql = nestatement;
			}
			pageHelper.appendValue(values);
		}

		Query<T> query = super.entitySqlQuery(sql, values);
		if (pageHelper != null) {
			PageThreadLocal.clearLocalPage(); // 清除分页条件
			int startIndex = pageHelper.getStartIndex();
			int pageSize = pageHelper.getPageSize();
			query.setFirstResult(startIndex).setMaxResults(pageSize);
		}
		page.setResult(query.list());
		return page;
	}

	@Override
	public <R> R getUniqueObjectBySql(StringBuilder sql, Map<String, Object> values) {
		NativeQuery<?> query = super.sqlQuery(sql, values);
		return (R) query.uniqueResult();
	}

	@Override
	public <R> R getUniqueGenericsBeanBySql(StringBuilder sql, Map<String, Object> values, Class<?> R) {
		NativeQuery<?> query = super.sqlQuery(sql, values);
		query.unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.aliasToBean(R));
		return (R) query.uniqueResult();
	}

	@Override
	public <R> R listGenericsBeanBySql(StringBuilder sql, Map<String, Object> values, Class<?> R) {
		NativeQuery<?> query = super.sqlQuery(sql, values);
		query.unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.aliasToBean(R));
		return (R) query.list();
	}

	@Override
	public <R> R pageGenericsBeanBySql(StringBuilder sql, Map<String, Object> values, Class<?> R) {
		StringBuilder totalCountSql = new StringBuilder("SELECT COUNT(*) FROM (" + sql + ") AS simon ");
		long totalCount = ((BigInteger) getUniqueObjectBySql(totalCountSql, values)).longValue();

		PageHelper pageHelper = PageThreadLocal.getLocalPage();
		Page<T> page = new Page<T>(pageHelper.getPage(), pageHelper.getPageSize());
		page.setTotalCount(totalCount);

		if (pageHelper != null && pageHelper.isAppendCondSwitch()) {
			StringBuilder nestatement = pageHelper.getStatements(sql);
			if (StringUtils.hasLength(nestatement)) {
				sql = nestatement;
			}
			pageHelper.appendValue(values);
		}

		NativeQuery<?> query = super.sqlQuery(sql, values);
		query.unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.aliasToBean(R));
		if (pageHelper != null) {
			PageThreadLocal.clearLocalPage(); // 清除分页条件
			int startIndex = pageHelper.getStartIndex();
			int pageSize = pageHelper.getPageSize();
			query.setFirstResult(startIndex).setMaxResults(pageSize);
		}
		page.setResultByView(query.list());
		return (R) page;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int executeBySql(StringBuilder sql, Map<String, Object> values) {
		return super.sqlQuery(sql, values).executeUpdate();
	}

	@Override
	public void updateFieldValueBySql(String field, Object origin, Object target) {
		StringBuilder statement = new StringBuilder(SQL.UPDATE);
		statement.append(this.getTableName());
		statement.append(SQL.SET); 
		statement.append(field).append(CHS.equal.getSymbol());
		if(target instanceof String) {
			statement.append(CHS.single_quotes.getSymbol() + target + CHS.single_quotes.getSymbol());
		}else {
			statement.append(target);
		}
		statement.append(SQL.WHERE).append(field).append(CHS.equal.getSymbol());
		if(origin instanceof String) {
			statement.append(CHS.single_quotes.getSymbol() + origin + CHS.single_quotes.getSymbol());
		}else {
			statement.append(origin);
		}
		super.sqlQuery(statement, null).executeUpdate();
	}
}
