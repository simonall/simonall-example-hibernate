package com.simonall.mysql.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;


/**
 * model基类
 * 
 * @author simon
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 409457884549551864L;

	// 主键
	private String id;

	// 创建时间
	private Date createTime;

	// 最近更新时间
	private Date updateTime;

	// 版本号 
	private Integer version;

	@Id
	@GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "uuid")
	@Column(name = "id", length = 36)
	public String getId() {
		return id;
	}

	// 2017-05-11 09:04:07.231551
	@Column(name = "create_time", nullable = false)
	public Date getCreateTime() {
		return createTime;
	}

	@Column(name = "update_time", nullable = true)
	public Date getUpdateTime() {
		return updateTime;
	}

	@Version
	@Column(nullable = false)
	public Integer getVersion() {
		return version;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}
	
}
