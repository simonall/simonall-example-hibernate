package com.simonall.mysql.service;

import java.io.Serializable;
import java.util.List;

public interface BaseService<T> {

	/**
	 * 保存数据
	 * 
	 * @param entity 实体类
	 */
	void save(T entity);

	/**
	 * 保存和更新数据
	 * 
	 * @param obj 实体对象
	 */
	void saveOrUpdate(T entity);

	/**
	 * 更新数据
	 * 
	 * @param entity 实体类
	 */
	void update(T entity);

	/**
	 * 删除数据
	 * 
	 * @param id 数据Id
	 */
	void deleteById(String... id);

	/**
	 * 根据ID获取记录
	 * 
	 * @param id 序列化ID
	 * @return 对象
	 */
	T getById(Serializable id);

	/**
	 * 获取记录集合
	 * 
	 * @return List
	 */
	List<T> getList();

	/**
	 * 根据属性名和属性值获取实体对象.
	 * 
	 * @param propertyName 属性名称
	 * @param value        属性值
	 * @return 实体对象
	 */
	T get(String propertyName, Object value);

	/**
	 * 根据属性名和属性值获取实体对象集合.
	 * 
	 * @param propertyName 属性名称
	 * @param value        属性值
	 * @return 实体对象集合
	 */
	List<T> getList(String propertyName, Object value);

	/**
	 * 查询指定字段判断数据是否存在
	 * 
	 * @param field 字段
	 * @param value 值
	 * @return boolean
	 */
	boolean isExist(String field, Object value);

	/**
	 * 根据属性名更新属性值
	 * 
	 * @param field  字段名称
	 * @param origin 源数据
	 * @param target 目标数据
	 */
	void updateFieldValueBySql(String field, Object origin, Object target);

}
