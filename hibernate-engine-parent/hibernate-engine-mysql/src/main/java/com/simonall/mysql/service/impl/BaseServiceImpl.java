package com.simonall.mysql.service.impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.simonall.mysql.dao.BaseDao;
import com.simonall.mysql.service.BaseService;

/**
 * 服务层基础类
 * 
 * @author simonall
 */
public class BaseServiceImpl<T> implements BaseService<T> {

	@Autowired
	private BaseDao<T> baseDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void save(T enity) {
		baseDao.save(enity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void saveOrUpdate(T entity) {
		baseDao.saveOrUpdate(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void update(T entity) {
		baseDao.update(entity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void deleteById(String... id) {
		if (id != null) {
			baseDao.deleteById(id);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public T getById(Serializable id) {
		return baseDao.getById(id);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<T> getList() {
		return baseDao.list();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public T get(String field, Object value) {
		return baseDao.get(field, value);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public List<T> getList(String field, Object value) {
		return baseDao.list(field, value);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
	public boolean isExist(String field, Object value) {
		return baseDao.isExist(field, value);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public void updateFieldValueBySql(String field, Object origin, Object target) {
		if(origin instanceof Double || origin instanceof BigDecimal) {
			baseDao.updateFieldValueBySql(field, origin, target);
		}else {
			baseDao.updateFieldValueBySql(field, String.valueOf(origin), String.valueOf(target));
		}
	}

}
