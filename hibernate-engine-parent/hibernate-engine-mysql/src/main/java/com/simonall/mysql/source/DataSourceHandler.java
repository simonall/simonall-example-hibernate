package com.simonall.mysql.source;

public class DataSourceHandler {

	private static ThreadLocal<String> dataSourceThredLocal = new ThreadLocal<String>();

	/**
	 * 设置当前线程使用数据源名称
	 * 
	 * @param datasource 接收当前线程数据源
	 */
	public static void setDataSource(String datasource) {
		dataSourceThredLocal.set(datasource);
	}

	/**
	 * 通过Key选择数据源
	 * 
	 * @return
	 */
	public static String getDataSource() {
		return dataSourceThredLocal.get();
	}

	/**
	 * 切换回默认的数据源
	 * 
	 */
	public static void clear() {
		dataSourceThredLocal.remove();
	}

}
