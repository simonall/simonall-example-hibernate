package com.simonall.mysql.source;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态切换数据源
 * 
 * @author JinQuan
 */
public class MultipleDataSource extends AbstractRoutingDataSource {

	/**
	 * 重写获取数据源Key的方法
	 * 
	 * @see AbstractRoutingDataSource#determineTargetDataSource()
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		return DataSourceHandler.getDataSource();
	}

}
