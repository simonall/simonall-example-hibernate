package com.simonall.rabbitmq;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * RabbitTemplate Initialize
 * 
 * @author simonall
 */
@Configuration
@ImportResource(locations = { "classpath:spring-rabbitmq.xml" })
public class RabbitTemplateInitialize {

}
