package com.simonall.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 【完全匹配型】通知
 * 
 * @author simon
 */
@Configuration
public class DirectInformConfig {

	// 交换机
	public static final String INFORM_EXCHANGE = "direct_inform_exchange";

	// 短信队列
	public static final String SMS_QUEUE = "inform_exchange_sms";

	// 邮件队列
	public static final String EMAIL_QUEUE = "inform_exchange_email";

	/**
	 * M:消息通知交换机
	 * 
	 * @return
	 */
	@Bean
	DirectExchange directExchange() {
		return new DirectExchange(INFORM_EXCHANGE);
	}

	/**
	 * M:短信发送消息队列
	 * 
	 * @return
	 */
	@Bean
	Queue sms() {
		return new Queue(SMS_QUEUE, true);
	}

	@Bean
	Binding bindingSms() {
		return BindingBuilder.bind(sms()).to(directExchange()).with(SMS_QUEUE);
	}

	/**
	 * M:邮件发送消息队列
	 * 
	 * @return
	 */
	@Bean
	Queue email() {
		return new Queue(EMAIL_QUEUE, true);
	}

	@Bean
	Binding bindingEmail() {
		return BindingBuilder.bind(email()).to(directExchange()).with(EMAIL_QUEUE);
	}

}
