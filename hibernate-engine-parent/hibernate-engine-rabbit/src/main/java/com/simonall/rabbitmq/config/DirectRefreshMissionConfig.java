package com.simonall.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 【广播型】刷新定时任务配置
 * 
 * @author simon
 */
@Configuration
public class DirectRefreshMissionConfig {

	// 广播交换机
	public static final String REFRESH_MISSION_EXCHANGE = "direct_mission_exchange";

	// 刷新配置队列
	public static final String REFRESH_MISSION_QUEUE = "mission_exchange_refresh";

	/**
	 * M:消息广播交换机
	 * 
	 * @return
	 */
	@Bean
	DirectExchange directRefreshMissionExchange() {
		return new DirectExchange(REFRESH_MISSION_EXCHANGE);
	}

	/**
	 * M:刷新任务调度配置队列
	 * 
	 * @return
	 */
	@Bean
	Queue refreshMission() {
		return new Queue(REFRESH_MISSION_QUEUE, true);
	}

	@Bean
	Binding bindingRefreshMission() {
		return BindingBuilder.bind(refreshMission()).to(directRefreshMissionExchange()).with(REFRESH_MISSION_QUEUE);
	}
}
