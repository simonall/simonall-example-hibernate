package com.simonall.rabbitmq.config;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 【广播型】刷新配置
 * 
 * @author simonall
 */
@Configuration
public class FanoutRefreshConfig {

	// 广播交换机
	public static final String REFRESH_CONFIG_EXCHANGE = "fanout_refresh_config_exchange";

	// 刷新配置队列
	public static final String REFRESH_CONFIG_QUEUE = "#{refreshConfig.name}";

	/**
	 * M:消息广播交换机
	 * 
	 * @return
	 */
	@Bean
	FanoutExchange fanoutRefreshConfigExchange() {
		return new FanoutExchange(REFRESH_CONFIG_EXCHANGE);
	}

	/**
	 * M:刷新配置队列
	 * 
	 * @return
	 */
	@Bean
	Queue refreshConfig() {
		return new AnonymousQueue();
	}

	@Bean
	Binding bindingRefreshConfig() {
		return BindingBuilder.bind(refreshConfig()).to(fanoutRefreshConfigExchange());
	}

}
