package com.simonall.rabbitmq.sender;

/**
 * Direct 队列方法
 * 
 * @author Administrator
 */
public interface DirectMessageSender {
	
	/**
	 * 发送刷新任务调度配置指令
	 * 
	 * DEMO:directMessageSender.sendRefreshMissionConfig(MissionIdentEnum.XXXXX);
	 * 
	 * @param message
	 * 				 任务调度标识
	 * @return
	 */
	public boolean sendRefreshMissionConfig(Object message);

}
