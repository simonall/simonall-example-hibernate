package com.simonall.rabbitmq.sender;

public interface FanoutMessageSender {
	
	/**
	 * 发送刷新配置指令
	 * 
	 * DEMO:fanoutMessageManager.sendRefreshConfig(DictValueEnum.ALIPAY_APP_ID);
	 * 
	 * @param message
	 * 			指令内容
	 * @return
	 */
	public boolean sendRefreshConfig(Object message);
	
}
