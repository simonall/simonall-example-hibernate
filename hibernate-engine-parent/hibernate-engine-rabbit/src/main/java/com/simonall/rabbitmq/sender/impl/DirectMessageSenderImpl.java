package com.simonall.rabbitmq.sender.impl;

import javax.annotation.Resource;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import com.simonall.rabbitmq.config.DirectRefreshMissionConfig;
import com.simonall.rabbitmq.sender.DirectMessageSender;

@Component
public class DirectMessageSenderImpl implements DirectMessageSender {
	
	@Resource
	private RabbitTemplate rabbitTemplate;
	
	@Override
	public boolean sendRefreshMissionConfig(Object message) {
		try {
			String exchange = DirectRefreshMissionConfig.REFRESH_MISSION_EXCHANGE;
			String routingKey = DirectRefreshMissionConfig.REFRESH_MISSION_QUEUE;
			rabbitTemplate.convertAndSend(exchange, routingKey, message);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
