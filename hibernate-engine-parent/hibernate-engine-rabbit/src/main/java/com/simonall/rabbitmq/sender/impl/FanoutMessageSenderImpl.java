package com.simonall.rabbitmq.sender.impl;

import javax.annotation.Resource;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import com.simonall.rabbitmq.config.FanoutRefreshConfig;
import com.simonall.rabbitmq.sender.FanoutMessageSender;

/**
 * 【订阅模式】消息队列
 * 
 * @author simonall
 */
@Component
public class FanoutMessageSenderImpl implements FanoutMessageSender {

	@Resource
	private RabbitTemplate rabbitTemplate;

	@Override
	public boolean sendRefreshConfig(Object message) {
		try {
			rabbitTemplate.convertAndSend(FanoutRefreshConfig.REFRESH_CONFIG_EXCHANGE, null, String.valueOf(message));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}


}
