package com.simonall.redis;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.simonall.redis.service.RedisCacheService;

import redis.clients.jedis.JedisPool;

/**
 * Redis Init 监听器
 * 
 * @author simon
 */
@Component
@WebListener
public class RedisInitialize extends ApplicationObjectSupport implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent event) {
		WebApplicationContext bean = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());

		// Redis单例
		RedisCacheService redisCacheService = bean.getBean(RedisCacheService.class);
		RedisSignleton.getInstance().initialize(redisCacheService);

		// Jedis单例
		JedisPool jedisPool = (JedisPool) bean.getBean("jedisPool");
		JedisSignleton.getInstance().initialize(jedisPool);
	}

	@Override
	public void contextDestroyed(ServletContextEvent envet) {
		// close application

	}

}
