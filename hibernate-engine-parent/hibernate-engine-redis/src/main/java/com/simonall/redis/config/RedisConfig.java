package com.simonall.redis.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;

/**
 * Init Redis
 * 
 * @author simon
 */
@Configuration
@Order(value = Integer.MIN_VALUE)
@ImportResource(locations = { "classpath:spring-data-redis.xml" })
public class RedisConfig {

}
