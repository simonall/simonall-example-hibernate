package com.simonall.redis.lock;

/**
 * Redis分布式锁Key
 * 
 * @author simon
 */
public enum DistributedKeyEnum {
	
	/**
	 * 购买网络代理锁
	 * 
	 */
	BUY_INTERNAL_NETWORK_AGENT_LOCK("LOCK:BUY_INTERNAL_NETWORK_AGENT")
	;
	
	private DistributedKeyEnum(String value) {
		this.value = value;
	}

	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
