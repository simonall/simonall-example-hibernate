package com.simonall.redis.lock;

import java.util.Collections;

import org.springframework.util.StringUtils;

import com.simonall.redis.JedisSignleton;

import redis.clients.jedis.Jedis;

/**
 * Redis 分布式锁
 * 
 * @author simonall
 */
public class RedisDistributedLock {

	private JedisSignleton jedisClient = JedisSignleton.getInstance();

	/**
	 * 数据插入成功返回OK
	 * 
	 */
	private static final String LOCK_VALUE = "OK";

	/**
	 * NX：即当key不存在时，进行set操作；若key已经存在，则不做任何操作
	 * 
	 */
	private static final String SET_IF_NOT_EXIST = "NX";

	/**
	 * 数据过期时间交给time处理；PX代表过期时间单位为毫秒
	 * 
	 */
	private static final String SET_WITH_EXPIRE_TIME = "PX";

	/**
	 * 释放锁成功返回的标识
	 * 
	 */
	private static final Long UN_LOCK_VALUE = 1L;

	/**
	 * 默认锁定时间
	 * 
	 */
	private static final int DEFAULT_LOCK_TIME = 10000;

	/**
	 * 重试获取锁的时间(ms)
	 * 
	 */
	private static final int RETRY_TIME_MILLISECOND = 50;

	/**
	 * 获取分布式锁
	 *
	 * @param key   锁key
	 * @param value 值
	 * @return 获取锁的状态
	 */
	public boolean lock(final String key, final String value) {
		if (!StringUtils.hasText(key) || !StringUtils.hasText(value)) {
			return false;
		}
		return lock(key, value, DEFAULT_LOCK_TIME);
	}

	/**
	 * 获取分布式锁
	 *
	 * @param key        锁key
	 * @param value      值
	 * @param expireTime 过期销毁时间
	 * @return 获取锁的状态
	 */
	public boolean lock(final String key, final String value, int expireTime) {
		Jedis jedis = jedisClient.getJedis();
		try {
			String result = jedis.set(key, value, SET_IF_NOT_EXIST, SET_WITH_EXPIRE_TIME, expireTime);
			if (LOCK_VALUE.equals(result)) {
				return true;
			}
			Thread.sleep(RETRY_TIME_MILLISECOND);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			jedisClient.close(jedis);
		}
		return lock(key, value, expireTime);
	}

	/**
	 * 释放分布式锁
	 *
	 * @param key   锁的KEY
	 * @param value 值
	 * @return 释放成功
	 */
	public boolean unLock(final String key, final String value, int count) {
		Jedis jedis = jedisClient.getJedis();
		try {
			if (count > 2) {
				return false;
			} // 尝试多次无果后释放递归
			String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
			Object result = jedis.eval(script, Collections.singletonList(key), Collections.singletonList(value));
			if (UN_LOCK_VALUE.equals(result)) {
				return true;
			}
			Thread.sleep(RETRY_TIME_MILLISECOND);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			jedisClient.close(jedis);
		}
		return unLock(key, value, ++count);
	}

}
