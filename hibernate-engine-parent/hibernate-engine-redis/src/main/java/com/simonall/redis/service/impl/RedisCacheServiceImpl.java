package com.simonall.redis.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.simonall.redis.service.RedisCacheService;

/**
 * Spring Redis Cache
 * 
 * @author simon
 */
@Component("redisCacheService")
public class RedisCacheServiceImpl implements RedisCacheService {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Override
	public boolean expire(String key, long time) {
		try {
			if (StringUtils.isNotBlank(key) && time > 0) {
				if (redisTemplate != null) {
					redisTemplate.expire(key, time, TimeUnit.SECONDS);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long getExpire(String key) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.getExpire(key, TimeUnit.SECONDS);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public boolean verifyKeyExist(String key) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.hasKey(key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void del(String... key) {
		try {
			if (key != null && key.length > 0) {
				if (key.length == 1) {
					redisTemplate.delete(key[0]);
				} else {
					@SuppressWarnings("unchecked")
					Collection<String> keys = CollectionUtils.arrayToList(key);
					redisTemplate.delete(keys);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object get(String key) {
		if (StringUtils.isNotBlank(key)) {
			if (redisTemplate != null) {
				return redisTemplate.opsForValue().get(key);
			}
		}
		return null;
	}

	@Override
	public boolean set(String key, Object value, long time) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long incr(String key, long delta) {
		if (StringUtils.isNotBlank(key) && delta > 0) {
			if (redisTemplate != null) {
				return redisTemplate.opsForValue().increment(key, delta);
			}
		}
		return 0;
	}

	@Override
	public long decr(String key, long delta) {
		if (delta < 0) {
			throw new RuntimeException("递减因子必须大于0");
		}
		return redisTemplate.opsForValue().increment(key, -delta);
	}

	@Override
	public Object hashMapGetValue(String key, String item) {
		if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(item)) {
			if (redisTemplate != null) {
				return redisTemplate.opsForHash().get(key, item);
			}
		}
		return new Object();
	}

	@Override
	public Map<Object, Object> hashMapGet(String key) {
		if (StringUtils.isNotBlank(key)) {
			if (redisTemplate != null) {
				return redisTemplate.opsForHash().entries(key);
			}
		}
		return new HashMap<Object, Object>();
	}

	@Override
	public boolean hashMapSet(String key, Map<String, Object> map, long time) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					redisTemplate.opsForHash().putAll(key, map);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.expire(key, time);
		}
		return false;
	}

	@Override
	public boolean hashMapSetValue(String key, String item, Object value, long time) {
		try {
			if (key != null && item != null) {
				if (redisTemplate != null) {
					redisTemplate.opsForHash().put(key, item, value);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.expire(key, time);
		}
		return false;
	}

	@Override
	public void hashMapDelValue(String key, Object... item) {
		try {
			if (StringUtils.isNotBlank(key) && item != null) {
				if (redisTemplate != null) {
					redisTemplate.opsForHash().delete(key, item);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean hashMapVerValueExist(String key, String item) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForHash().hasKey(key, item);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public double hincr(String key, String item, double by) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForHash().increment(key, item, by);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0.0;
	}

	@Override
	public double hdecr(String key, String item, double by) {
		return redisTemplate.opsForHash().increment(key, item, -by);
	}

	@Override
	public Set<Object> setGet(String key) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForSet().members(key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashSet<Object>();
	}

	@Override
	public boolean setVerValueExist(String key, Object value) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForSet().isMember(key, value);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long setSetValue(String key, long time, Object... values) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForSet().add(key, values);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.expire(key, time);
		}
		return 0;
	}

	@Override
	public long setGetSize(String key) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForSet().size(key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public long setRemoveValue(String key, Object... values) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForSet().remove(key, values);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<Object> listGet(String key, long start, long end) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForList().range(key, start, end);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<Object>();
	}

	@Override
	public long listGetSize(String key) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForList().size(key);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Object listGetIndexValue(String key, long index) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForList().index(key, index);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Object();
	}

	@Override
	public boolean listSet(String key, List<Object> value, long time) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					redisTemplate.opsForList().rightPushAll(key, value);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			this.expire(key, time);
		}
		return false;
	}

	@Override
	public boolean listUpdateIndexValue(String key, long index, Object value) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					redisTemplate.opsForList().set(key, index, value);
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long listRemoveValue(String key, long count, Object value) {
		try {
			if (StringUtils.isNotBlank(key)) {
				if (redisTemplate != null) {
					return redisTemplate.opsForList().remove(key, count, value);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
