package com.simonall.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;

/**
 * RocketMQ Init
 * 
 * @author simonall
 */
@Configuration
@Order(value = Integer.MIN_VALUE)
@ImportResource(locations = { "classpath:spring-mq-rocket.xml" })
public class RocketConfig {
	
	
}
