package com.simonall.initialize;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.context.support.ApplicationObjectSupport;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.simonall.sender.RocketSenderSignleton;

/**
 * Initialize RocketSender
 * 
 * @author simonall
 */
@Component
@WebListener
public class RocketSenderInitialize extends ApplicationObjectSupport implements ServletContextListener {
	
	@Override
	public void contextInitialized(ServletContextEvent event) {
		WebApplicationContext bean = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
		
		// Rocket MQ Template
		RocketMQTemplate template = bean.getBean(RocketMQTemplate.class);
		RocketSenderSignleton.getInstance().initialize(template);
	}

	@Override
	public void contextDestroyed(ServletContextEvent envet) {
		// close application

	}
}
