package com.simonall.sender;

import org.apache.rocketmq.spring.core.RocketMQTemplate;

import com.simonall.sender.beans.SystemLogDTO;
import com.simonall.sender.beans.ThrowableDTO;
import com.simonall.sender.config.SystemExceptionQueue;
import com.simonall.sender.config.SystemExecptionAccessQueue;

/**
 * Rocket Sender Signleton
 * 
 * @author simonall
 */
public class RocketSenderSignleton {
	
	private static RocketSenderSignleton instance = new RocketSenderSignleton();

	public static RocketSenderSignleton getInstance() {
		return instance;
	}
	
	private RocketMQTemplate template;
	
	public void initialize(RocketMQTemplate template) {
		this.template = template;
	}
	
	/**
	 * 记录系统异常
	 * 
	 * @param e
	 */
	public final void senderThrowable(Throwable e) {
		template.convertAndSend(SystemExceptionQueue.TOPIC, ThrowableDTO.build(e));
	}
	
	/**
	 * 记录系统异常接口信息
	 * 
	 * @param data
	 */
	public final void senderSystemExecptionAccess(SystemLogDTO data) {
		template.convertAndSend(SystemExecptionAccessQueue.TOPIC, data);
	}

}
