package com.simonall.sender.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统日志DTO
 * 
 * @author simonall
 */
public class SystemLogDTO implements Serializable {

	private static final long serialVersionUID = 1627427271053777142L;

	// 创建时间
	private Date createTime;

	// 请求地址
	private String requestUrl;

	// 请求IP
	private String ip;

	// 请求参数
	private String requestParam;

	// 请求方式
	private String requestMethod;

	// 请求头
	private String requestHead;

	// 响应参数
	private String responseParam;

	// 响应头
	private String responseHead;

	// 异常信息
	private String exceptionDetail;

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getRequestParam() {
		return requestParam;
	}

	public void setRequestParam(String requestParam) {
		this.requestParam = requestParam;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getRequestHead() {
		return requestHead;
	}

	public void setRequestHead(String requestHead) {
		this.requestHead = requestHead;
	}

	public String getResponseParam() {
		return responseParam;
	}

	public void setResponseParam(String responseParam) {
		this.responseParam = responseParam;
	}

	public String getResponseHead() {
		return responseHead;
	}

	public void setResponseHead(String responseHead) {
		this.responseHead = responseHead;
	}

	public String getExceptionDetail() {
		return exceptionDetail;
	}

	public void setExceptionDetail(String exceptionDetail) {
		this.exceptionDetail = exceptionDetail;
	}

	public static SystemLogDTO build() {
		return null;
	}
}
