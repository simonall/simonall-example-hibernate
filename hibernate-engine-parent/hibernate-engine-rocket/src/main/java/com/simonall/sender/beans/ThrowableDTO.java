package com.simonall.sender.beans;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

/**
 * 异常消息
 * 
 * @author simonall
 */
public class ThrowableDTO implements Serializable {

	private static final long serialVersionUID = 3325253899423029410L;
	
	// 标题
	private String title;
	
	// 简述内容
	private String content;
	
	// 通知详情
	private String extendDetail;
	
	// 扩展参数
	private String bizParam;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getExtendDetail() {
		return extendDetail;
	}

	public void setExtendDetail(String extendDetail) {
		this.extendDetail = extendDetail;
	}

	public String getBizParam() {
		return bizParam;
	}

	public void setBizParam(String bizParam) {
		this.bizParam = bizParam;
	}
	
	public static final ThrowableDTO build(Throwable e) {
		e.printStackTrace();
		// 1.写入问题到日子对象
		StringWriter input = new StringWriter();
		
		// 2.创建写出对象
		PrintWriter out = new PrintWriter(input);
		
		// 3.把错误信息写入错误对象中
		e.printStackTrace(out);
		
		ThrowableDTO result = new ThrowableDTO();
		result.setTitle(e.getLocalizedMessage());
		result.setContent(e.toString());
		result.setExtendDetail(input.toString());
		return result;
	}

}
