package com.simonall.sender.config;

/**
 * System Exception Queue
 * 
 * @author simonall
 */
public class SystemExceptionQueue {
	
	/**
	 * System Exception Topic
	 * 
	 */
	public static final String TOPIC = "TOPIC_SYSTEM_EXCEPTION";
	
	/**
	 * System Exception Consumer
	 * 
	 */
	public static final String CONSUMER = "CONSUMER_SYSTEM_EXCEPTION";
}
