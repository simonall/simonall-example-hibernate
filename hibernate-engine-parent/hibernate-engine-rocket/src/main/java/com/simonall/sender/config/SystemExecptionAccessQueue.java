package com.simonall.sender.config;

public class SystemExecptionAccessQueue {

	/**
	 * Execption Access Topic
	 * 
	 */
	public static final String TOPIC = "TOPIC_EXECPTION_ACCESS";

	/**
	 * Execption Access Consumer
	 * 
	 */
	public static final String CONSUMER = "CONSUMER_EXECPTION_ACCESS";

}
