package com.simonall.search.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;

/**
 * Elastic Search
 * 
 * @author simonall
 */
@Configuration
@Order(value = Integer.MIN_VALUE)
@ImportResource(locations = { "classpath:spring-data-search.xml" })
public class SearchConfig {

}
