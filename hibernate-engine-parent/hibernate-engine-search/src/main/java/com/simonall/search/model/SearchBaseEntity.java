package com.simonall.search.model;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * @Field 注解说明：
 * 		index : 是否使用分词；默认为true
 * 		analyzer : 存储时使用的分词器
 * 		searchAnalyzer : 查询时使用的分词器
 * 		store : 是否存储
 * 		type : 数据类型
 * @author simonall
 */
public class SearchBaseEntity implements Serializable {

	private static final long serialVersionUID = 1381098242331819334L;
	
	@Id
	@Field(index = false, type = FieldType.Auto)
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
