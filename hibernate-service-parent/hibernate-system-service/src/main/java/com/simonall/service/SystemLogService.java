package com.simonall.service;

import com.simonall.mysql.model.SystemLog;
import com.simonall.mysql.service.BaseService;

public interface SystemLogService extends BaseService<SystemLog> {

}
